import React from 'react'
import logo from 'assets/image/keuraisdesk.png'

function MendawaiLogo({ className }) {
    return (
        <>
            <img src={logo} alt="Keuraisdesk-Icon" className={className} />
            {/* <img src="/assets/media/keuraisdesk.png" alt="Keuraisdesk-Icon" className={className} /> */}
        </>
    )
}

export default MendawaiLogo
