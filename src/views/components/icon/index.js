import IconMailInbox from './IconMailInbox';
import IconMailSent from './IconMailSent';
import IconMailMarked from './IconMailMarked';
import IconMailSpam from './IconMailSpam';

export {
    IconMailInbox,
    IconMailSent,
    IconMailMarked,
    IconMailSpam,
}