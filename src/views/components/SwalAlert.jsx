import Swal from 'sweetalert2';

function SwalAlertSuccess(title, text) {
    Swal.fire({
        title: title,
        text: text,
        buttonsStyling: false,
        icon: "success",
        confirmButtonText: "Ok",
        customClass: {
            confirmButton: "btn btn-primary"
        },
        timer: 1500
    });
}

function SwalAlertError(title, text) {
    Swal.fire({
        title: title,
        text: text,
        buttonsStyling: false,
        icon: "warning",
        confirmButtonText: "Ok",
        customClass: {
            confirmButton: "btn btn-primary"
        },
    });
}

export {
    SwalAlertSuccess,
    SwalAlertError,
}