import React from 'react'
import { useDispatch } from 'react-redux'
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import CustomStore from 'devextreme/data/custom_store';
import axios from 'axios';

import Icons from 'views/components/Icons'
import { ButtonCreate, ButtonEdit, ButtonRefresh } from 'views/components/button';
import { Card, CardBody, CardHeader, CardToolbar } from 'views/components/card'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { apiCustomerList } from 'app/services/apiCustomer'

const TestCustomer = () => {
    const dispatch = useDispatch();
    // const { customers } = useSelector(state => state.customer);
    // const [param, setParams] = useState('');

    // useEffect(() => {
    //     dispatch(apiCustomerList(param))
    // }, [dispatch,param.skip]);

    function isNotEmpty(value) {
        return value !== undefined && value !== null && value !== '';
    }

    const store = new CustomStore({
        key: 'id',
        load: async (loadOptions) => {
            let params = '?';
            [
                'skip',
                'take',
                'requireTotalCount',
                'requireGroupCount',
                'sort',
                'filter',
                'totalSummary',
                'group',
                'groupSummary',
            ].forEach((i) => {
                if (i in loadOptions && isNotEmpty(loadOptions[i])) { params += `${i}=${JSON.stringify(loadOptions[i])}&`; }
            });
            params = params.slice(0, -1);

            return await axios.get(`/customer/data_grid${params}`)
                .then((data) => ({
                    data: data.data,
                    totalCount: data.totalCount,
                }))
                .catch(() => { throw new Error('Data Loading Error'); });
        },
    });

    function componentButtonActions(data) {
        const { customer_id } = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <ButtonEdit to={`customer/${customer_id}/edit`} />
            </div>
        )
    }

    return (
        <MainContent>
            <SubHeader active_page="Customer" menu_name="Data Customer" modul_name="Customer">
                <ButtonCreate to="/customer/create" />
            </SubHeader>
            <Container>
                <Card>
                    <CardHeader className="border-0">
                        <CardToolbar>
                            <ul className="nav nav-light-primary nav-bold nav-pills">
                                <li className="nav-item">
                                    <a className="nav-link active" data-toggle="tab" href="#tabDataCustomer" >
                                        <span className="nav-icon">
                                            <Icons iconName="layer" className="svg-icon svg-icon-sm" />
                                        </span>
                                        <span className="nav-text">Data Customer</span>
                                    </a>
                                </li>
                            </ul>
                        </CardToolbar>
                        <CardToolbar>
                            <ButtonRefresh onClick={() => dispatch(apiCustomerList())} />
                        </CardToolbar>
                    </CardHeader>
                    <CardBody>
                        <div className="tab-content">
                            <div className="tab-pane fade active show" id="tabDataCustomer" role="tabpanel" aria-labelledby="tabDataCustomer">
                                <DataGrid
                                    dataSource={store}
                                    remoteOperations={true}
                                    allowColumnReordering={true}
                                    allowColumnResizing={true}
                                    columnAutoWidth={true}
                                    showBorders={true}
                                    showColumnLines={true}
                                    showRowLines={true}
                                >
                                    <HeaderFilter visible={true} />
                                    <FilterRow visible={true} />
                                    <Paging defaultPageSize={10} />
                                    <Pager
                                        visible={true}
                                        allowedPageSizes={[10, 20, 50]}
                                        displayMode='full'
                                        showPageSizeSelector={true}
                                        showInfo={true}
                                        showNavigationButtons={true} />
                                    <Column caption="Actions" dataField="id" width={100} cellRender={componentButtonActions} />
                                    <Column caption="CustomerID" dataField="customer_id" />
                                    <Column caption="Name" dataField="name" />
                                    <Column caption="Email" dataField="email" />
                                    <Column caption="Phone Number" dataField="telephone" />
                                    <Column caption="NIK" dataField="no_ktp" />
                                    <Column caption="Gender" dataField="gender" />
                                    <Column caption="Address" dataField="address" />
                                    <Column caption="Status" dataField="status" cellRender={(data) => {
                                        return <span className={`label label-md label-light-${data.value === 'Registered' ? 'success' : 'warning'} label-inline`}>{data.value}</span>
                                    }} />
                                </DataGrid>
                            </div>
                        </div>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default TestCustomer
