import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as echarts from 'echarts';

import { ButtonRefresh } from 'views/components/button';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import IconBrand from 'views/components/IconBrand';
import { apiDashSocmed_AgentOnline, apiDashSocmed_Top10Chat, apiDashSocmed_TotalChannel } from 'app/services/apiDashboard';

function DashboardSocmed() {
    const chartDom = useRef();
    const dispatch = useDispatch();
    const { agent_online, total_channel, top10_chat } = useSelector(state => state.dashboard);

    useEffect(() => {
        function LoadChart() {
            const myChart = echarts.init(chartDom.current);
            const option = {
                title: {
                    text: ''
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['Whatsapp', 'Chat', 'Facebook', 'Messenger', 'Twitter', 'Instagram']
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                },
                yAxis: {
                    type: 'value'
                },
                series: [
                    {
                        name: 'Whatsapp',
                        type: 'bar',
                        smooth: true,
                        data: [12, 13, 10, 14, 15, 23, 21]
                    },
                    {
                        name: 'Chat',
                        type: 'bar',
                        smooth: true,
                        data: [20, 18, 19, 24, 20, 21, 10]
                    },
                    {
                        name: 'Facebook',
                        type: 'bar',
                        smooth: true,
                        data: [10, 22, 20, 14, 10, 30, 10]
                    },
                    {
                        name: 'Messenger',
                        type: 'bar',
                        smooth: true,
                        data: [20, 7, 30, 15, 5, 21, 20]
                    },
                    {
                        name: 'Instagram',
                        type: 'bar',
                        smooth: true,
                        data: [10, 6, 12, 8, 32, 13, 10]
                    }, {
                        name: 'Twitter',
                        type: 'bar',
                        smooth: true,
                        data: [12, 22, 6, 21, 9, 13, 12]
                    }
                ]
            }
            option && myChart.setOption(option);
        }
        LoadChart()
    }, [])

    useEffect(() => {
        dispatch(apiDashSocmed_AgentOnline())
        dispatch(apiDashSocmed_TotalChannel())
        dispatch(apiDashSocmed_Top10Chat())
    }, [dispatch])

    function RefreshData() {
        dispatch(apiDashSocmed_AgentOnline())
        dispatch(apiDashSocmed_TotalChannel())
        dispatch(apiDashSocmed_Top10Chat())
    }

    return (
        <MainContent>
            <SubHeader active_page="Dashboard" menu_name="Dashboard" modul_name="Ticket">
                <ul className="nav nav-pills nav-pills-sm nav-dark-75 mx-5">
                    <li className="nav-item">
                        <a className="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_11_1">Month</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_11_2">Week</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link py-2 px-4 active" data-toggle="tab" href="#kt_tab_pane_11_3">Day</a>
                    </li>
                </ul>
                <ButtonRefresh onClick={() => RefreshData()} />
            </SubHeader>
            <Container>
                <div className="row">
                    {
                        total_channel.map((item, index) => {
                            return (
                                <div className="col-md-3" key={index}>
                                    <div className={`card card-custom card-stretch gutter-b`}>
                                        <div className="card-body">
                                            <div className="symbol symbol-45 symbol-circle border-1">
                                                <div className="symbol-label">
                                                    <IconBrand name={item.channel} height={45} width={45} />
                                                </div>
                                            </div>
                                            <span className="card-title font-weight-bolder  font-size-h2 mb-0 d-block">{item.total}</span>
                                            <span className="font-weight-bold  font-size-sm">{item.channel}</span>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>

                <div className="row mb-10">
                    <div className="col-lg-8">
                        <div className="card p-0">
                            <div className="card-header">
                                <div className='d-flex align-items-center justify-content-between'>
                                    <div>Activity Detail</div>
                                    <span className="label label-light-primary label-inline mx-2">156</span>
                                </div>
                            </div>
                            <div className="card-body bg-white" style={{ height: '350px', width: '100%' }} ref={chartDom} />
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card">
                            <div className="card-header">
                                <div className='d-flex align-items-center justify-content-between'>
                                    <div>Agent Online</div>
                                    <span className="label label-light-primary label-inline mx-2">{agent_online?.length}</span>
                                </div>
                            </div>
                            <div className="card-body bg-white" style={{ height: '350px', width: '100%' }}>
                                {
                                    agent_online.map((item, index) => {
                                        return (
                                            <div className="list list-hover mb-4" key={index}>
                                                <div className='list-item d-flex align-items-center justify-content-between'>
                                                    <div className="d-flex align-items-center py-2 mx-2">
                                                        <div className="symbol symbol-40px">
                                                            <div className="symbol-label bg-light-success">
                                                                <i className="fa fa-user text-success"></i>
                                                            </div>
                                                        </div>
                                                        <div className="flex-grow-1 mx-2">
                                                            <div className="font-weight-bolder mr-2">{item.username}</div>
                                                            <div className="mt-2">max concurent ({item.max_whatsapp})</div>
                                                        </div>
                                                    </div>
                                                    <div className="d-flex flex-column align-items-end mx-2">
                                                        <div className="text-muted">Handle</div>
                                                        <div className="d-flex align-items-center justify-content-between mt-2">
                                                            <span className="label label-light-primary font-weight-bold label-inline mx-2">{item.total_handle}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mb-20">
                    <div className="col-lg-12">
                        <div className="card p-0">
                            <div className="card-header">
                                <div className='d-flex align-items-center justify-content-between'>
                                    <div>Top 10 Last Chat.</div>
                                </div>
                            </div>
                            <div className="card-body bg-white">
                                <table className="table">
                                    <thead className="datatable-head">
                                        <tr className="datatable-row" style={{ left: 0 }}>
                                            <th className="datatable-cell datatable-cell-sort"><span style={{ width: 132 }}>Chat ID</span></th>
                                            <th className="datatable-cell datatable-cell-sort"><span style={{ width: 132 }}>Channel</span></th>
                                            <th className="datatable-cell datatable-cell-sort"><span style={{ width: 132 }}>Customer Identity</span></th>
                                            <th className="datatable-cell datatable-cell-sort"><span style={{ width: 132 }}>Customer Name</span></th>
                                            <th className="datatable-cell datatable-cell-sort"><span style={{ width: 132 }}>Message</span></th>
                                            <th className="datatable-cell datatable-cell-sort"><span style={{ width: 132 }}>Datetime</span></th>
                                            <th className="datatable-cell datatable-cell-sort"><span style={{ width: 132 }}>Status</span></th>
                                            <th className="datatable-cell datatable-cell-sort"><span style={{ width: 132 }}>Agent Handle</span></th>
                                        </tr>
                                    </thead>
                                    <tbody className="datatable-body" style={{ height: '300px', width: '100%', overflow: 'auto' }}>
                                        {
                                            top10_chat.map((item, index) => {
                                                return (
                                                    <tr className="datatable-row" key={index}>
                                                        <td className="datatable-cell"><span>{item.chat_id}</span></td>
                                                        <td className="datatable-cell"><span style={{ width: 132 }}>{item.channel}</span></td>
                                                        <td className="datatable-cell"><span style={{ width: 132 }}>{item.user_id}</span></td>
                                                        <td className="datatable-cell"><span style={{ width: 132 }}>{item.name}</span></td>
                                                        <td className="datatable-cell"><span style={{ width: 132 }}>{item.message}</span></td>
                                                        <td className="datatable-cell"><span style={{ width: 132 }}>{item.date_create}</span></td>
                                                        <td className="datatable-cell">
                                                            <span style={{ width: 132 }}>
                                                            <span className="label font-weight-bold label-lg label-light-primary label-inline">{item.status_chat}</span>
                                                            </span>
                                                        </td>
                                                        <td className="datatable-cell"><span style={{ width: 132 }}>{item.agent_handle}</span></td>
                                                    </tr>
                                                )
                                            })
                                        }

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </Container >
        </MainContent >
    )
}

export default DashboardSocmed
