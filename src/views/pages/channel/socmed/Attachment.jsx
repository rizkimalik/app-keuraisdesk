import React from 'react'

function Attachment({ value }) {
    return (
        <div className="d-flex align-items-center">
            {
                value?.map((item, index) => {
                    return (
                        <div className="border border-dashed border-primary rounded p-5" key={index}>
                            <div className="d-flex flex-aligns-center pe-10 pe-lg-20 mx-4">
                                <div className="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">
                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle font-size-20">
                                        <i className="fa fa-paperclip align-middle" />
                                    </div>
                                </div>
                                <div className="ms-1 fw-semibold">
                                    <a href={item.file_url} target="_blank" rel="noopener noreferrer" className="fs-6 text-hover-primary fw-bold">{item.file_origin}</a>
                                    <div className="text-gray-400">{item.file_size / 1000} kb</div>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default Attachment