import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';
import { socket, urlAttachment } from 'app/config';
import Datetime from 'views/components/Datetime';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { authUser } from 'app/slice/sliceAuth';
import { getListCustomer, getLoadConversation, getEndChat } from 'app/services/apiSosmed';
import { setSelectedCustomer } from 'app/slice/sliceSosmed';
import ListCustomer from './ListCustomer';
import Conversation from './Conversation';
import FormSendMessage from './FormSendMessage';
import { TicketCreate } from 'views/pages/ticket';
import CustomerJourney from 'views/pages/customer/CustomerJourney';
import { setTicketThread } from 'app/slice/sliceTicket';

const SocialMedia = () => {
    const dispatch = useDispatch();
    const btnCreateTicketRef = useRef(null);
    const [message, setMessage] = useState('');
    const { username } = useSelector(authUser);
    const [conversation, setConversation] = useState([]);
    const [attachment, setAttachment] = useState('');
    const [typing, setTyping] = useState(false);
    const { selected_customer, conversations } = useSelector(state => state.sosialmedia);

    useEffect(() => {
        socket.on('return-typing', (res) => {
            setTyping(res.typing);
        });
    }, []);

    useEffect(() => {
        function showMessageContent(res) {
            if (selected_customer) {
                if (res.customer_id === selected_customer.customer_id) {
                    setConversation(conversation => [...conversation, res]);
                }
            }
            dispatch(getListCustomer({ agent_handle: username }))
        }

        socket.on('send-message-customer', (res) => { //push from blending
            showMessageContent(res);
        });
        socket.on('return-message-customer', (res) => {
            showMessageContent(res);
        });
        socket.on('return-message-whatsapp', (res) => {
            showMessageContent(res);
        });

        dispatch(getListCustomer({ agent_handle: username }))
    }, [dispatch, username, selected_customer]);

    useEffect(() => {
        setConversation(conversations.data)
    }, [conversations]);


    function sendMessage(e) {
        e.preventDefault();

        let message_type = 'text';
        let attachment_file = '';
        if (attachment) {
            let generate_filename = Date.now() + '.' + (attachment.name).split('.').pop();
            message_type = (attachment.type).split('/')[0] === 'image' ? 'image' : 'document';
            attachment_file = [{
                attachment: attachment,
                file_origin: attachment?.name,
                file_name: generate_filename,
                file_size: attachment?.size,
                file_url: attachment ? `${urlAttachment}/${selected_customer.channel}/${generate_filename}` : '',
            }];
        }

        let values = {
            username: selected_customer.agent_handle,
            chat_id: selected_customer.chat_id,
            user_id: selected_customer.user_id,
            customer_id: selected_customer.customer_id,
            message: message,
            message_type: message_type,
            name: username,
            email: selected_customer.email,
            channel: selected_customer.channel,
            flag_to: 'agent',
            agent_handle: username,
            date_create: Datetime(),
            uuid_customer: socket.id,
            uuid_agent: selected_customer.uuid_agent,
            attachment: attachment_file,
            typing: false
        }

        if (message) {
            if (values.channel === 'Chat') {
                socket.emit('send-message-agent', values);
            }
            else if (values.channel === 'Whatsapp') {
                socket.emit('send-message-whatsapp', values);
            }
            setConversation(conversation => [...conversation, values]);
        }
        setMessage('');
        setAttachment('');
    }

    function onKeyTyping() {
        let data = {
            uuid_customer: socket.id,
            uuid_agent: selected_customer.uuid_agent,
            email: selected_customer.email,
            agent_handle: selected_customer.agent_handle,
            flag_to: 'agent',
            typing: true
        }
        socket.emit('typing', data);
    }

    function handlerSelectCustomer(customer) {
        dispatch(setSelectedCustomer(customer))
        dispatch(getLoadConversation({
            chat_id: customer.chat_id,
            customer_id: customer.customer_id
        }))
    }

    function handlerEndChat(value) {
        const { chat_id, customer_id, user_id, channel } = value;
        Swal.fire({
            title: 'End Session',
            text: 'Do you want to close the chat?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'End Session',
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire('Success!', '', 'success')
                dispatch(setTicketThread({
                    thread_id: chat_id,
                    thread_channel: channel,
                    account: user_id,
                    subject: 'interaction data sosmed.'
                }));
                
                btnCreateTicketRef.current.click();
                dispatch(getEndChat({ chat_id, customer_id }))
                dispatch(getListCustomer({ agent_handle: username }))
                dispatch(setSelectedCustomer({}))
                dispatch(getLoadConversation({ chat_id, customer_id }))
            }
        })
    }

    return (
        <MainContent>
            <SubHeader active_page="Channels" menu_name="Social Media" modul_name="Omnichannel" />
            <Container>
                <div className="d-flex flex-row">
                    <ListCustomer handlerSelectCustomer={handlerSelectCustomer} />
                    <div className="flex-row-fluid ml-lg-4">
                        <Card>
                            <CardHeader className="border-bottom">
                                <CardTitle
                                    title={selected_customer?.name}
                                    subtitle={selected_customer?.email && typing ? 'Typing..' : selected_customer?.email}
                                />
                                <button type="button" ref={btnCreateTicketRef} className="hide" data-toggle="modal" data-target="#modalCreateTicket">create ticket</button>
                                {
                                    selected_customer?.chat_id &&
                                    <CardToolbar>
                                        <button type="button" className="btn btn-icon btn-sm btn-light-info btn-circle ml-3" title="Journey Customer" data-toggle="modal" data-target="#modalJourneyCustomer">
                                            <i className="fas fa-route fa-sm"></i>
                                        </button>
                                        <button type="button" className="btn btn-icon btn-sm btn-light-primary btn-circle ml-2" title="Customer Profile" onClick={(e) => alert('profile')}><i className="fa fa-id-card fa-sm"></i></button>
                                        <button type="button" className="btn btn-icon btn-sm btn-light-danger btn-circle ml-2" title="End Chat" onClick={(e) => handlerEndChat(selected_customer)}>
                                            <i className="far fa-window-close fa-sm"></i>
                                        </button>
                                    </CardToolbar>
                                }
                            </CardHeader>
                            <CardBody className="p-0">
                                <Conversation conversation={conversation} />
                            </CardBody>

                            <FormSendMessage
                                message={message}
                                setMessage={setMessage}
                                attachment={attachment}
                                setAttachment={setAttachment}
                                sendMessage={sendMessage}
                                onKeyTyping={onKeyTyping}
                            />
                        </Card>
                    </div>
                </div>
                {selected_customer.customer_id && <CustomerJourney customer={selected_customer} />}
                {
                    //? Create ticket after end session
                    selected_customer.customer_id &&
                    <TicketCreate customer={selected_customer} />
                }
            </Container>



        </MainContent>
    )
}

export default SocialMedia