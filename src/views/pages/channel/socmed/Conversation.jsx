import React from 'react'
import Attachment from './Attachment';

function Conversation({ conversation }) {
    return (
        <div data-mobile-height={350} style={{ height: 'calc(75vh - 160px)', overflow: 'auto' }}>
            <div className="messages p-8">
                {
                    conversation?.map((item, index) => {
                        if (item.flag_to === 'customer') {
                            return (
                                <div className="d-flex justify-content-start mb-10" key={index}>
                                    <div className="d-flex flex-column align-items-start">
                                        <div className="d-flex align-items-center mb-2">
                                            <div className="symbol symbol-35px symbol-circle">
                                                <div className="symbol-label bg-light-primary fw-bolder">
                                                    <i className="fa fa-id-card text-primary"></i>
                                                </div>
                                            </div>
                                            <div className="ml-3">
                                                <span className="fs-5 fw-bolder text-gray-900 text-hover-primary">{item.name}</span>
                                                <span className="text-muted mb-1 mx-2">{item.date_create}</span>
                                            </div>
                                        </div>
                                        <div className="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">
                                            {item.attachment && <Attachment value={item.attachment} />}
                                            {item.message}
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                        else if (item.flag_to === 'agent') {
                            return (
                                <div className="d-flex justify-content-end mb-10" key={index}>
                                    <div className="d-flex flex-column align-items-end">
                                        <div className="d-flex align-items-center mb-2">
                                            <div className="mr-3">
                                                <span className="text-muted mb-1 mx-2">{item.date_create}</span>
                                                <span className="fs-5 fw-bolder text-gray-900 text-hover-primary ms-1">{item.name}</span>
                                            </div>
                                            <div className="symbol symbol-35px symbol-circle">
                                                <div className="symbol-label bg-light-gray fw-bolder">
                                                    <i className="fa fa-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-end" data-kt-element="message-text">
                                            {item.attachment && <Attachment value={item.attachment} />}
                                            {item.message}
                                        </div>
                                    </div>
                                </div>
                            )
                        }

                        return ('');
                    })
                }
            </div>
        </div>
    )
}

export default Conversation