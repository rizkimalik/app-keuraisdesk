import React from 'react'
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

function EmailInbox() {

    function componentButtonActions(data) {
        // const { customer_id } = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button className="btn btn-icon btn-xs btn-hover-text-warning active" data-toggle="tooltip" data-placement="right" title data-original-title="Star">
                    <i className="flaticon-star text-muted" />
                </button>
            </div>
        )
    }

    return (
        <div className="border rounded p-4 my-2">
            <DataGrid
                dataSource=""
                keyExpr="id"
                remoteOperations={{
                    filtering: true,
                    sorting: true,
                    paging: true
                }}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={10} />
                <Pager
                    visible={true}
                    allowedPageSizes={[10, 20, 50]}
                    displayMode='full'
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Actions" dataField="id" width={100} cellRender={componentButtonActions} />
                <Column caption="Subject" dataField="ticket_number" />
                <Column caption="From" dataField="channel" />
                <Column caption="Agent" dataField="status" />
                <Column caption="Date Email" dataField="user_create" />
                <Column caption="Date Assign" dataField="created_at" />
            </DataGrid>
        </div>
    )
}

export default EmailInbox