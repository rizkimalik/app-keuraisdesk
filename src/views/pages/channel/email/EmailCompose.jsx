import React from 'react'
import { NavLink } from 'react-router-dom'
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card'
import { IconMailInbox } from 'views/components/icon'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'

function EmailCompose() {
    return (
        <MainContent>
            <SubHeader active_page="Channels" menu_name="Email" modul_name="Compose" />
            <Container>
                <div className="row">
                    <div className="col-md-12 col-sm-12">
                        <Card>
                            <CardHeader className="border-bottom">
                                <CardTitle title="Compose" subtitle="" />
                                <CardToolbar>
                                    <NavLink to="/channel/email" className="btn btn-primary font-weight-bolder btn-sm m-1">
                                        <IconMailInbox className="svg-icon svg-icon-sm" /> Inbox
                                    </NavLink>
                                </CardToolbar>
                            </CardHeader>
                            <CardBody className="px-5">
                                <form id="kt_inbox_compose_form">
                                    <div className="d-block">
                                        <div className="d-flex align-items-center border-bottom inbox-to px-8 min-h-45px">
                                            <div className="text-dark-50 w-75px">To:</div>
                                            <div className="d-flex align-items-center flex-grow-1">
                                                <input type="text" className="form-control border-0" name="compose_to" />
                                            </div>
                                        </div>
                                        <div className="d-flex align-items-center border-bottom inbox-to px-8 min-h-45px">
                                            <div className="text-dark-50 w-75px">Cc:</div>
                                            <div className="d-flex align-items-center flex-grow-1">
                                                <input type="text" className="form-control border-0" name="" />
                                            </div>
                                        </div>
                                        <div className="border-bottom">
                                            <input className="form-control border-0 px-8 min-h-45px" name="compose_subject" placeholder="Subject" />
                                        </div>
                                        {/* <div className="ql-toolbar ql-snow px-5 border-top-0 border-left-0 border-right-0"><span className="ql-formats"><span className="ql-header ql-picker"><span className="ql-picker-label" tabIndex={0} role="button" aria-expanded="false" aria-controls="ql-picker-options-1"><svg viewBox="0 0 18 18"> <polygon className="ql-stroke" points="7 11 9 13 11 11 7 11" /> <polygon className="ql-stroke" points="7 7 9 5 11 7 7 7" /> </svg></span><span className="ql-picker-options" aria-hidden="true" tabIndex={-1} id="ql-picker-options-1"><span tabIndex={0} role="button" className="ql-picker-item" data-value={1} /><span tabIndex={0} role="button" className="ql-picker-item" data-value={2} /><span tabIndex={0} role="button" className="ql-picker-item" data-value={3} /><span tabIndex={0} role="button" className="ql-picker-item" /></span></span><select className="ql-header" style={{ display: 'none' }}><option value={1} /><option value={2} /><option value={3} /><option selected="selected" /></select></span><span className="ql-formats"><button type="button" className="ql-bold"><svg viewBox="0 0 18 18"> <path className="ql-stroke" d="M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z" /> <path className="ql-stroke" d="M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z" /> </svg></button><button type="button" className="ql-italic"><svg viewBox="0 0 18 18"> <line className="ql-stroke" x1={7} x2={13} y1={4} y2={4} /> <line className="ql-stroke" x1={5} x2={11} y1={14} y2={14} /> <line className="ql-stroke" x1={8} x2={10} y1={14} y2={4} /> </svg></button><button type="button" className="ql-underline"><svg viewBox="0 0 18 18"> <path className="ql-stroke" d="M5,3V9a4.012,4.012,0,0,0,4,4H9a4.012,4.012,0,0,0,4-4V3" /> <rect className="ql-fill" height={1} rx="0.5" ry="0.5" width={12} x={3} y={15} /> </svg></button><button type="button" className="ql-link"><svg viewBox="0 0 18 18"> <line className="ql-stroke" x1={7} x2={11} y1={7} y2={11} /> <path className="ql-even ql-stroke" d="M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z" /> <path className="ql-even ql-stroke" d="M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z" /> </svg></button></span><span className="ql-formats"><button type="button" className="ql-list" value="ordered"><svg viewBox="0 0 18 18"> <line className="ql-stroke" x1={7} x2={15} y1={4} y2={4} /> <line className="ql-stroke" x1={7} x2={15} y1={9} y2={9} /> <line className="ql-stroke" x1={7} x2={15} y1={14} y2={14} /> <line className="ql-stroke ql-thin" x1="2.5" x2="4.5" y1="5.5" y2="5.5" /> <path className="ql-fill" d="M3.5,6A0.5,0.5,0,0,1,3,5.5V3.085l-0.276.138A0.5,0.5,0,0,1,2.053,3c-0.124-.247-0.023-0.324.224-0.447l1-.5A0.5,0.5,0,0,1,4,2.5v3A0.5,0.5,0,0,1,3.5,6Z" /> <path className="ql-stroke ql-thin" d="M4.5,10.5h-2c0-.234,1.85-1.076,1.85-2.234A0.959,0.959,0,0,0,2.5,8.156" /> <path className="ql-stroke ql-thin" d="M2.5,14.846a0.959,0.959,0,0,0,1.85-.109A0.7,0.7,0,0,0,3.75,14a0.688,0.688,0,0,0,.6-0.736,0.959,0.959,0,0,0-1.85-.109" /> </svg></button><button type="button" className="ql-list" value="bullet"><svg viewBox="0 0 18 18"> <line className="ql-stroke" x1={6} x2={15} y1={4} y2={4} /> <line className="ql-stroke" x1={6} x2={15} y1={9} y2={9} /> <line className="ql-stroke" x1={6} x2={15} y1={14} y2={14} /> <line className="ql-stroke" x1={3} x2={3} y1={4} y2={4} /> <line className="ql-stroke" x1={3} x2={3} y1={9} y2={9} /> <line className="ql-stroke" x1={3} x2={3} y1={14} y2={14} /> </svg></button></span><span className="ql-formats"><button type="button" className="ql-clean"><svg className viewBox="0 0 18 18"> <line className="ql-stroke" x1={5} x2={13} y1={3} y2={3} /> <line className="ql-stroke" x1={6} x2="9.35" y1={12} y2={3} /> <line className="ql-stroke" x1={11} x2={15} y1={11} y2={15} /> <line className="ql-stroke" x1={15} x2={11} y1={11} y2={15} /> <rect className="ql-fill" height={1} rx="0.5" ry="0.5" width={7} x={2} y={14} /> </svg></button></span></div><div id="kt_inbox_compose_editor" className="border-0 ql-container ql-snow" style={{ height: 250 }}><div className="ql-editor ql-blank px-8" data-gramm="false" contentEditable="true" data-placeholder="Type message..."><p><br /></p></div><div className="ql-clipboard" contentEditable="true" tabIndex={-1} /><div className="ql-tooltip ql-hidden"><a className="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank" /><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL" /><a className="ql-action" /><a className="ql-remove" /></div></div> */}
                                        
                                    </div>
                                    <div className="d-flex align-items-center justify-content-between py-5 pl-8 pr-5 border-top">
                                        <div className="d-flex align-items-center mr-3">
                                            <div className="btn-group mr-4">
                                                <span className="btn btn-primary font-weight-bold px-6">Send</span>
                                                <span className="btn btn-primary font-weight-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="button" />
                                                <div className="dropdown-menu dropdown-menu-sm dropup p-0 m-0 dropdown-menu-right">
                                                    <ul className="navi py-3">
                                                        <li className="navi-item">
                                                            <div className="navi-link">
                                                                <span className="navi-icon">
                                                                    <i className="flaticon2-hourglass-1" />
                                                                </span>
                                                                <span className="navi-text">Cancel</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <span className="btn btn-icon btn-sm btn-clean mr-2 dz-clickable">
                                                <i className="flaticon2-clip-symbol" />
                                            </span>
                                        </div>
                                        <div className="d-flex align-items-center">
                                            <span className="btn btn-icon btn-sm btn-clean" data-inbox="dismiss" data-toggle="tooltip" title="Delete" data-original-title="Dismiss reply">
                                                <i className="flaticon2-rubbish-bin-delete-button" />
                                            </span>
                                        </div>
                                    </div>
                                </form>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>



    )
}

export default EmailCompose