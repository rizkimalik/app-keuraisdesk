import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { ButtonRefresh } from 'views/components/button'
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card'
import { IconMailInbox, IconMailMarked, IconMailSent, IconMailSpam } from 'views/components/icon'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'

import EmailInbox from './EmailInbox'
import EmailSent from './EmailSent'

function Email() {
    const [navigate, setNavigate] = useState('Inbox');

    return (
        <MainContent>
            <SubHeader active_page="Channels" menu_name="Email" modul_name={navigate} />
            <Container>
                <div className="row">
                    <div className="col-md-3 col-sm-12">
                        <Card>
                            <CardBody className="px-5">
                                <div className="px-4 mt-4 mb-10">
                                    <NavLink to="/channel/email/compose" className="btn btn-block btn-primary btn-sm font-weight-bold text-uppercase text-center">Compose</NavLink>
                                </div>

                                <div className="navi navi-hover navi-active navi-link-rounded navi-bold navi-icon-center navi-light-icon">
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabEmailInbox" onClick={(e) => setNavigate('Inbox')} className={`navi-link ${navigate === 'Inbox' ?? 'active'}`}>
                                            <span className="navi-icon mr-4">
                                                <IconMailInbox className="svg-icon svg-icon-lg" />
                                            </span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Inbox</span>
                                            <span className="navi-label">
                                                <span className="label label-rounded label-primary font-weight-bolder">0</span>
                                            </span>
                                        </a>
                                    </div>
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabEmailSent" onClick={(e) => setNavigate('Sent')} className={`navi-link ${navigate === 'Sent' ?? 'active'}`}>
                                            <span className="navi-icon mr-4">
                                                <IconMailSent className="svg-icon svg-icon-lg" />
                                            </span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Sent</span>
                                        </a>
                                    </div>
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabEmailMarked" onClick={(e) => setNavigate('Marked')} className={`navi-link ${navigate === 'Marked' ?? 'active'}`}>
                                            <span className="navi-icon mr-4">
                                                <IconMailMarked className="svg-icon svg-icon-lg" />
                                            </span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Marked</span>
                                        </a>
                                    </div>
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabEmailSpam" onClick={(e) => setNavigate('Spam')} className={`navi-link ${navigate === 'Spam' ?? 'active'}`}>
                                            <span className="navi-icon mr-4">
                                                <IconMailSpam className="svg-icon svg-icon-lg" />
                                            </span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Spam</span>
                                        </a>
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="offcanvas-mobile-overlay" />
                    <div className="col-md-9 col-sm-12">
                        <Card>
                            <CardHeader className="border-bottom">
                                <CardTitle title={`Data Email ${navigate}`} subtitle="" />
                                <CardToolbar>
                                    <ButtonRefresh onClick={() => alert('refresh')} />
                                </CardToolbar>
                            </CardHeader>
                            <CardBody className="p-4">
                                <div className="tab-content">
                                    <div className="tab-pane fade active show" id="tabEmailInbox" role="tabpanel" aria-labelledby="tabDataCustomer">
                                        {navigate === 'Inbox' && <EmailInbox />}
                                    </div>
                                    <div className="tab-pane fade" id="tabEmailSent" role="tabpanel" aria-labelledby="tabEmailSent">
                                        {navigate === 'Sent' && <EmailSent />}
                                    </div>
                                    <div className="tab-pane fade" id="tabEmailMarked" role="tabpanel" aria-labelledby="tabEmailMarked">
                                        <div>Marked</div>
                                    </div>
                                    <div className="tab-pane fade" id="tabEmailSpam" role="tabpanel" aria-labelledby="tabEmailSpam">
                                        <div>spam</div>
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default Email