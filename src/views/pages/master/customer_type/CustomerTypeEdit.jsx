import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { apiCustomerTypeShow, apiCustomerTypeUpdate } from 'app/services/apiCustomerType'
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { authUser } from 'app/slice/sliceAuth';
import FormInput from 'views/components/FormInput';

function CustomerTypeEdit() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { id } = useParams();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        async function getCustomerTypeShow() {
            try {
                const { payload } = await dispatch(apiCustomerTypeShow({ id }))
                if (payload.status === 200) {
                    const {
                        id,
                        type_name,
                        active,
                        description
                    } = payload.data;
                    reset({
                        id,
                        type_name,
                        description,
                        active
                    });
                }
            }
            catch (error) {
                console.log(error);
            }
        }
        getCustomerTypeShow();
    }, [id, dispatch, reset]);

    const onSubmitEditCustomerType = async (data) => {
        try {
            data.updated_by = username;
            const { payload } = await dispatch(apiCustomerTypeUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data.');
                history.push('/customer_type');
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', 'Please try again!')
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Customer Type" modul_name="Customer Type Edit" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form Update Customer Type" subtitle="Form update customer_type." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitEditCustomerType)} className="form">
                        <CardBody>
                            <FormInput
                                name="type_name"
                                type="text"
                                label="Customer Type"
                                className="form-control"
                                placeholder="Enter type name"
                                register={register}
                                rules={{ required: true, maxLength: 100 }}
                                readOnly={false}
                                errors={errors.type_name}
                            />
                            <FormInput
                                name="description"
                                type="textarea"
                                label="Description"
                                className="form-control"
                                placeholder="Enter Description"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.description}
                            />
                            <FormInput
                                name="active"
                                type="checkbox"
                                label="Active"
                                className="switch switch-outline switch-icon switch-sm switch-primary ml-4"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.active}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/customer_type" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default CustomerTypeEdit