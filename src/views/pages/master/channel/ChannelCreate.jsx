import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { apiChannelList, apiChannelStore } from 'app/services/apiChannel';
import { authUser } from 'app/slice/sliceAuth';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import FormInput from 'views/components/FormInput';

function ChannelCreate() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { register, formState: { errors }, handleSubmit } = useForm();
    const { channels } = useSelector(state => state.channel)

    useEffect(() => {
        dispatch(apiChannelList())
    }, [dispatch]);

    const onSubmitCreateChannel = async (data) => {
        try {
            data.created_by = username;
            const { payload } = await dispatch(apiChannelStore(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Insert Success', 'Success into application.');
                history.push('/channel')
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', 'Please try again!')
        }
    }


    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Channel Ticket" modul_name="Channel Create" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form Add Channel" subtitle="Form add new channel." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitCreateChannel)} className="form">
                        <CardBody>
                            <FormInput
                                name="channel"
                                type="datalist"
                                list="channel_list"
                                datalist={channels.map((item) => item.channel)}
                                label="Channel Ticket"
                                className="form-control"
                                placeholder="Enter Channel"
                                register={register}
                                rules={{ required: true, maxLength: 100 }}
                                readOnly={false}
                                errors={errors.channel}
                            />
                            <FormInput
                                name="channel_type"
                                type="text"
                                label="Channel Type"
                                className="form-control"
                                placeholder="Enter Channel Type"
                                register={register}
                                rules={{ required: true, maxLength: 100 }}
                                readOnly={false}
                                errors={errors.channel_type}
                            />
                            <FormInput
                                name="description"
                                type="textarea"
                                label="Description"
                                className="form-control"
                                placeholder="Enter Description"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.description}
                            />
                            <FormInput
                                name="active"
                                type="checkbox"
                                label="Active"
                                className="switch switch-outline switch-icon switch-sm switch-primary ml-4"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.active}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/channel" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default ChannelCreate