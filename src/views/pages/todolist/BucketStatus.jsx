import React from 'react'
import { NavLink } from 'react-router-dom';
import Icons from 'views/components/Icons';

function BucketStatus({ total_ticket }) {
    return (
        <div className="row">
            {
                total_ticket.map((item, index) => {
                    let background, icon;
                    if (item.total === 0) {
                        background = 'bg-warning'
                        icon = 'flag'
                    } else {
                        background = 'bg-primary'
                        icon = 'open'
                    }

                    return (
                        <div className="col-md-3" key={index}>
                            <NavLink to={`/todolist/${item.status}`} className={`card card-custom ${background} card-stretch gutter-b`}>
                                <div className="card-body">
                                    <Icons iconName={`${icon}`} className="svg-icon svg-icon-3x svg-icon-white" />
                                    <span className="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{item.total}</span>
                                    <span className="font-weight-bold text-white font-size-sm">Ticket {item.status}</span>
                                </div>
                            </NavLink>
                        </div>
                    )
                })
            }

            {/* <div className="col-md-3">
                        <NavLink to="" className="card card-custom bg-success card-stretch gutter-b">
                            <div className="card-body">
                                <Icons iconName="open" className="svg-icon svg-icon-2x svg-icon-white" />
                                <span className="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">0</span>
                                <span className="font-weight-bold text-white font-size-sm">Ticket Open</span>
                            </div>
                        </NavLink>
                    </div>
                    <div className="col-md-3">
                        <NavLink to="" className="card card-custom bg-warning card-stretch gutter-b">
                            <div className="card-body">
                                <Icons iconName="refresh" className="svg-icon svg-icon-2x svg-icon-white" />
                                <span className="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">0</span>
                                <span className="font-weight-bold text-white font-size-sm">Ticket Pending</span>
                            </div>
                        </NavLink>
                    </div>
                    <div className="col-md-3">
                        <NavLink to="" className="card card-custom bg-info card-stretch gutter-b">
                            <div className="card-body">
                                <Icons iconName="equalizer" className="svg-icon svg-icon-2x svg-icon-white" />
                                <span className="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">0</span>
                                <span className="font-weight-bold text-white font-size-sm">Ticket Progress</span>
                            </div>
                        </NavLink>
                    </div>
                    <div className="col-md-3">
                        <NavLink to="" className="card card-custom bg-primary card-stretch gutter-b">
                            <div className="card-body">
                                <Icons iconName="flag" className="svg-icon svg-icon-2x svg-icon-white" />
                                <span className="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 text-hover-primary d-block">0</span>
                                <span className="font-weight-bold text-white font-size-sm">Ticket Closed</span>
                            </div>
                        </NavLink>
                    </div> */}
        </div>
    )
}

export default BucketStatus