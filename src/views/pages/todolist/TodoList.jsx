import React, { useEffect } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { useDispatch, useSelector } from 'react-redux'

import Icons from 'views/components/Icons'
import { ButtonRefresh } from 'views/components/button'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { Card, CardBody, CardHeader, CardTitle } from 'views/components/card'
import { authUser } from 'app/slice/sliceAuth'
import { apiTodolistDataTicket, apiTodolistTotalTicket } from 'app/services/apiTodolist'
import { TicketUpdate } from 'views/pages/ticket'
import { apiTicketShow } from 'app/services/apiTicket'
import BucketStatus from './BucketStatus'


function TodoList() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { total_ticket, data_ticket } = useSelector(state => state.todolist);
    const { status } = useParams();
    const user = useSelector(authUser);

    useEffect(() => {
        if (status === undefined || status === '') {
            history.push('/todolist/Open')
        }

        const data = {
            user_create: user.username,
            department_id: user.department,
            organization_id: user.organization,
            user_level: user.user_level,
            status: status
        }
        dispatch(apiTodolistTotalTicket(data));
        dispatch(apiTodolistDataTicket(data));
    }, [dispatch, status, history, user]);

    const onDispatchTodolist = () => {
        const data = {
            user_create: user.username,
            department_id: user.department,
            organization_id: user.organization,
            user_level: user.user_level,
            status: status
        }
        dispatch(apiTodolistTotalTicket(data));
        dispatch(apiTodolistDataTicket(data));
    }

    return (
        <MainContent>
            <SubHeader active_page="Todolist" menu_name="Todolist" modul_name="Task">
                <ButtonRefresh onClick={() => onDispatchTodolist()} />
            </SubHeader>
            <Container>
                <BucketStatus total_ticket={total_ticket} />

                <div className="row">
                    <div className="col-lg-12">
                        <Card>
                            <CardHeader className="border-0">
                                <CardTitle title={`Data Ticket ${status}`} subtitle="Data Todolist Ticket." />
                            </CardHeader>
                            <CardBody>
                                <DataGrid
                                    dataSource={data_ticket}
                                    keyExpr="id"
                                    allowColumnReordering={true}
                                    allowColumnResizing={true}
                                    columnAutoWidth={true}
                                    showBorders={true}
                                    showColumnLines={true}
                                    showRowLines={true}
                                >
                                    <HeaderFilter visible={true} />
                                    <FilterRow visible={true} />
                                    <Paging defaultPageSize={10} />
                                    <Pager
                                        visible={true}
                                        displayMode='full'
                                        allowedPageSizes={[10, 20, 50]}
                                        showPageSizeSelector={true}
                                        showInfo={true}
                                        showNavigationButtons={true} />
                                    <Column caption="Ticket Number" dataField="ticket_number" cellRender={(data) => {
                                        return <button type="button" onClick={(e) => dispatch(apiTicketShow({ ticket_number: data.value }))} className="btn btn-sm btn-light-primary py-1 px-2" data-toggle="modal" data-target="#modalUpdateTicket">
                                            <Icons iconName="ticket" className="svg-icon svg-icon-sm p-0" />
                                            {data.value}
                                        </button>
                                    }} />
                                    <Column caption="Ticket on Layer" dataField="ticket_position" />
                                    <Column caption="Group Ticket" dataField="group_ticket_number" />
                                    <Column caption="CustomerID" dataField="customer_id" />
                                    <Column caption="Name" dataField="name" />
                                    <Column caption="Channel" dataField="ticket_source" />
                                    <Column caption="Date Create" dataField="date_create" />
                                    <Column caption="Status" dataField="status" />
                                    <Column caption="Category" dataField="category_name" />
                                    <Column caption="Category Product" dataField="category_sublv1_name" />
                                    <Column caption="Category Case" dataField="category_sublv2_name" />
                                    <Column caption="Category Detail" dataField="category_sublv3_name" />
                                    <Column caption="SLA (Days)" dataField="sla" />
                                    <Column caption="Department" dataField="organization_name" />
                                    <Column caption="Complaint Detail" dataField="complaint_detail" />
                                    <Column caption="Response Detail" dataField="response_detail" />
                                </DataGrid>
                            </CardBody>
                        </Card>
                    </div>
                </div>
                <TicketUpdate />
            </Container>
        </MainContent>
    )
}

export default TodoList
