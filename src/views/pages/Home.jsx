import React from 'react'
import { Card, CardBody } from 'views/components/card'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import ticketing from 'assets/image/ticketing.png'


const Home = () => {
    return (
        <MainContent>
            <SubHeader active_page="KeuraisDesk" menu_name="Welcome" modul_name="App KeuraisDesk" />
            <Container>
                <div className="row">
                    <div className="col-lg-12 col-md-12">
                        <Card>
                            <CardBody className="d-flex align-items-center">
                                <div className="text-center">
                                    <p className="display-4 text-info">Keurais Desk</p>
                                    <p>Contact center solutions allow your contact center to connect with customers on any communication channel like voice call, video call, live chat, SMS, social media, and more. Provide customer service on any channel and seamlessly switch among any digital channels during an interaction, while maintaining context and relevant information across all channels as if it were a single conversation.</p>
                                    <img src={ticketing} alt="Mendawai-Ticketing" style={{ width: '100%', maxWidth: 600, height: 'auto' }} />
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default Home
