import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getListCustomer = createAsyncThunk(
    "sosialmedia/getListCustomer",
    async ({ agent_handle }) => {
        const res = await axios.post('/omnichannel/list_customers', { agent_handle });
        return res.data;
    }
)

export const getLoadConversation = createAsyncThunk(
    "sosialmedia/getLoadConversation",
    async ({ chat_id, customer_id }) => {
        const res = await axios.post('/omnichannel/conversation_chats', { chat_id, customer_id });
        return res.data;
    }
)

export const getEndChat = createAsyncThunk(
    "sosialmedia/getEndChat",
    async ({ chat_id, customer_id }) => {
        const res = await axios.post('/omnichannel/end_chat', { chat_id, customer_id });
        return res.data;
    }
)