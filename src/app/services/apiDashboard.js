import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";


export const apiDashSocmed_AgentOnline = createAsyncThunk(
    "dashboard/apiDashSocmed_AgentOnline",
    async () => {
        const res = await axios.get('/dashboard/socmed/agent_online');
        return res.data;
    }
)

export const apiDashSocmed_TotalChannel = createAsyncThunk(
    "dashboard/apiDashSocmed_TotalChannel",
    async () => {
        const res = await axios.get('/dashboard/socmed/total_channel');
        return res.data;
    }
)

export const apiDashSocmed_Top10Chat = createAsyncThunk(
    "dashboard/apiDashSocmed_Top10Chat",
    async () => {
        const res = await axios.get('/dashboard/socmed/top10_chat');
        return res.data;
    }
)
