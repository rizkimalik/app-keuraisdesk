import { createSlice } from "@reduxjs/toolkit";
import { apiDashSocmed_AgentOnline, apiDashSocmed_TotalChannel, apiDashSocmed_Top10Chat } from "app/services/apiDashboard";


const sliceDashboard = createSlice({
    name: "dashboard",
    initialState: {
        agent_online: [],
        total_channel: [],
        top10_chat: [],
    },
    extraReducers: {
        [apiDashSocmed_AgentOnline.fulfilled]: (state, action) => {
            state.agent_online = action.payload.data
        },
        [apiDashSocmed_TotalChannel.fulfilled]: (state, action) => {
            state.total_channel = action.payload.data
        },
        [apiDashSocmed_Top10Chat.fulfilled]: (state, action) => {
            state.top10_chat = action.payload.data
        },
    },
});

export default sliceDashboard;